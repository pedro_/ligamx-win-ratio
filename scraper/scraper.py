import os
from scrapy.crawler import CrawlerProcess
import spiders.hgspidermt as hgspidermt
import spiders.hgspidervef as hgspidervef
import spiders.hgspidermtlogos as hgspidermtlogos

# Press Ctrl+F5 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.




# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print(__name__)
    process = CrawlerProcess()
    # process.crawl(hgspidermt.MTCalendarSpider)
    process.crawl(hgspidervef.VEFCalendarSpider, opt='all_single_file')
    #process.crawl(hgspidervef.VEFCalendarSpider, opt='all_multi_file')
    #process.crawl(hgspidervef.VEFCalendarSpider, opt='latest')
    process.start()

    # Check option to download logos
    # TODO: even though mediotiempo.com is a good source to get the logos, we won't get the logos from teams that have
    #       disappeared from the LigaMX first division. We need to get another source that has great logos but all those
    #       who have been part of the LigaMX.

    logos_process = CrawlerProcess()
    logos_process.crawl(hgspidermtlogos.MTLigaMXLogosSpider)
    logos_process.start()