import os
import numpy as np
import matplotlib as mplib
import matplotlib.offsetbox
import matplotlib.pyplot as plt
from ligamx_statsrep import plotutils as hgpltutils
from ligamx_statsrep import datautils as hgdatautils


HOME_AWAY_PLOTS_REL_DIR = 'artifacts/plots/home_away_wins'


def add_teams_logos_to_axes_pie(ax, team1, team2, texts, logos_pos='up', zoom=0.1, roffset=0.25):
    """
    Add logos to the Axes.Pie wins plot.

    Args:
        ax (matplotlib.Axes):
        team1 (str): Must correspond to the first element on the 'texts' list.
        team2 (str): Must correspond to the last element on the 'texts' list.
        texts (list of matplotlib.text.Text): Object returned by matplotlib.axes.Axes.pie. Position of the logo
                                              depends on this data
        logos_pos (str, opt): 'up'; upper corners. 'middle': sides of the plot.
                              'low': lower corners. 'data': aligned with the values on the plot.
        zoom (float, opt): Scale applied to the logo image.
        roffset (float, opt): Offset radial to the Pie plot. Used to separate more the logo if needed.

    Returns:
        None

    """
    assert (len(texts) == 3)

    theta = np.pi/8
    coord_x = np.cos(theta)
    coord_y = np.sin(theta)

    print("coords: ", coord_x, coord_y)

    if 'up' == logos_pos:
        logo_pos = ((-coord_x - roffset, coord_y + roffset),
                    (coord_x + roffset, coord_y + roffset))
    elif 'center' == logos_pos:
        logo_pos = ((-1 - roffset, 0), (1 + roffset, 0))
    elif 'low' == logos_pos:
        logo_pos = ((-coord_x - roffset, -coord_y - roffset), (coord_x + roffset, -coord_y - roffset))
    elif 'data' == logos_pos:
        logo_pos = (texts[0].get_position(), texts[2].get_position())
    else:
        raise ValueError("Invalid value for 'logos_pos' parameter.")

    print('logo_pos: ', logo_pos)

    for i, team in enumerate((team1, team2)):
        team_logo_filename = os.path.join(hgpltutils.LOGOS_DIR, hgpltutils.LOGOS_DICT[team])

        # Load the image data
        with mplib.cbook.get_sample_data(team_logo_filename) as file:
            arr_img = plt.imread(file, format='png')

        imagebox = mplib.offsetbox.OffsetImage(arr_img, zoom=zoom)
        imagebox.image.axes = ax

        print(' logo_pos[i]: ', logo_pos[i])
        box_x, box_y = logo_pos[i]

        if logos_pos == 'data':
            if box_x > 0:
                box_x += roffset
            else:
                box_x -= roffset

            if box_y > 0:
                box_y += roffset
            else:
                box_y -= roffset

        boxprops = {"facecolor": "none", "edgecolor": "none"}

        ab = mplib.offsetbox.AnnotationBbox(imagebox, (box_x, box_y),
                                            frameon=False,
                                            xybox=(box_x, box_y),
                                            xycoords='data',
                                            boxcoords="data",
                                            pad=0.5,
                                            bboxprops=boxprops)

        ax.add_artist(ab)


def pie_plot_wins_home_away(df, team1, team2, phase='regular', title='', title_fontsize=20, fig_size=(10, 10), save_fig=None):
    """

    Args:
        df (pandas.DataFrame):
        team1 (str):
        team2 (str):
        title (str, optional):
        title_fontsize (int, optional):
        fig_size (tuple int, optional):
        save_fig (str, optional):

    Returns
        tuple of objects: (matplotlib.figure.Figure, matplotlib.axes.Axes)
    """

    # ------------------------------------------------
    #  Helper functions
    # ------------------------------------------------
    # Compute the value that will be displayed instead of the percentage
    def func(pct, all_vals):
        """
        Compute the value based on the percentage and the sum of all values.
        """
        if abs(pct > 0.000000000001):
            absolute = int(np.rint((pct / 100.0) * np.sum(all_vals)))
            return "{:d}".format(absolute)
        else:
            return ""

    # ------------------------------------------------

    # Create a list of dictionaries that contain the results for each phase. The first element contains the info of
    # ALL phases.
    direct_matches_dict = hgdatautils.get_wins_direct_matches_dict(df, team1, team2, phase=phase)

    # Create a np.Array 2D. Keep only home and away results.
    # NOTE: here we order in a different than the dictionary since when it's plotted
    #       we want to display team1 wins and team2 wins next to each other separated
    #       by the draws.
    direct_matches = np.array((direct_matches_dict[team1], direct_matches_dict['Empate'], direct_matches_dict[team2]))

    results_colors = hgpltutils.get_results_colors_for_pie(team1, team2)
    home_away_colors = hgpltutils.get_home_away_wins_colors_for_pie(team1, team2)

    # ------------------------------------------------
    #  Plot - Total wins + home and away
    # ------------------------------------------------
    fig, ax1 = plt.subplots()
    fig.set_size_inches(fig_size)
    # fig.tight_layout()

    # TODO: I think this has to be replaced by text that we relocate.
    # fig.suptitle(f'{team1} vs {team2}\n'
    #              f'{direct_matches[:, 2].sum()} juegos de {hgpltutils.get_phase_text_for_title(phase)} en torneos cortos',
    #              fontsize=title_fontsize)

    # Background color
    # fig.set_facecolor('#f6f7f2')
    fig.set_facecolor('#f0f0f0')

    #
    # Common settings
    #
    wedge_size = 0.35
    wedge_edge_color = '#ffffffff'
    wedge_line_width = 3
    text_size_outer = 20
    text_size_inner = 18
    text_pos_outer = 0.82
    text_pos_inner = 0.72

    # TODO: I think this has to be replaced by text that we relocate.
    if title == '':
        title = f'{team1} vs {team2}\n' \
                f'{direct_matches[:, 2].sum()} juegos de {hgpltutils.get_phase_text_for_title(phase)} en torneos cortos\n' \
                f'Victorias como local y visitante'

    ax1.set_title(title, fontdict={'fontsize': title_fontsize})
    # ax1.set_title(f'Local y Visitante', fontdict={'fontsize': title_fontsize - 4})

    print(direct_matches.shape)
    print(direct_matches[0])
    print(direct_matches[:, 2])
    print(direct_matches[:, 2].flatten())

    patches1, texts1, autotexts1 = ax1.pie(direct_matches[:, 2].flatten(),  # labels=(team1, 'Empates', team2),
                                           autopct=lambda pct: func(pct, direct_matches[:, 2].flatten()),
                                           radius=1,
                                           wedgeprops=dict(width=wedge_size, edgecolor=wedge_edge_color, linewidth=wedge_line_width),
                                           colors=results_colors,
                                           startangle=90,
                                           pctdistance=text_pos_outer,
                                           textprops=dict(color="w", fontsize=text_size_outer, ))

    patches2, texts2, autotexts2 = ax1.pie(direct_matches[:, :2].flatten(),
                                           # labels=('H', 'A')*direct_matches[0].shape[0],
                                           autopct=lambda pct: func(pct, direct_matches[:, :2].flatten()),
                                           radius=1 - wedge_size,
                                           wedgeprops=dict(width=wedge_size, edgecolor=wedge_edge_color, linewidth=wedge_line_width),
                                           colors=home_away_colors,
                                           startangle=90,
                                           pctdistance=text_pos_inner,
                                           textprops=dict(color="w", fontsize=text_size_inner))

    for i, text in enumerate(autotexts1):
        if i == 0:
            text.set_color(hgpltutils.LIGAMX_COLORS_TEXT[team1])
        elif i == 2:
            text.set_color(hgpltutils.LIGAMX_COLORS_TEXT[team2])
        else:
            text.set_color(hgpltutils.LIGAMX_COLORS_TEXT['None'])

        text.set_ha('center')
        text.set_va('center')

    for i, text in enumerate(autotexts2):
        if i < 4:
            if i % 2 == 0:
                team = team1
            else:
                team = team2
        else:
            if i % 2 == 0:
                team = team2
            else:
                team = team1

        text.set_color(hgpltutils.LIGAMX_COLORS_TEXT[team])
        text.set_ha('center')
        text.set_va('center')

    # ------------------------------------------------
    #  Common tweaks
    # ------------------------------------------------

    #  Display the value
    # ------------------------------------------------
    for wedge1 in patches1:
        wedge1.set_alpha(1)

    for wedge in patches2:
        wedge.set_alpha(0.97)

    # ------------------------------------------------
    #  Add logos in plot
    # ------------------------------------------------
    add_teams_logos_to_axes_pie(ax1, team1, team2, texts1, logos_pos='up', zoom=0.2)

    # ------------------------------------------------
    # Save the figure
    # ------------------------------------------------
    if save_fig:
        hgpltutils.create_dir(HOME_AWAY_PLOTS_REL_DIR)
        filename = os.path.join(HOME_AWAY_PLOTS_REL_DIR, f'{team1}_v_{team2}_home_away_{phase}.png')
        print(f'Saving image in: {filename}')
        fig.savefig(filename, transparent=True)

    plt.show()

    return fig, ax1


# NOTE: Old pie plot for liguilla wins
#       It will be replaced by the stacked bar plot in hgligamxbarplotter.py. BUT I leave it here just in case I don't
#       like the result.
def pie_plot_wins_liguilla(df, team1, team2, title='', title_fontsize=20, fig_size=(10, 10), save_fig=None):
    """

    Returns:
        tuple of objects: (matplotlib.figure.Figure, matplotlib.axes.Axes)

    """

    # ------------------------------------------------
    #  Helper functions
    # ------------------------------------------------
    # Compute the value that will be displayed instead of the percentage
    def func(pct, all_vals):
        """
        Compute the value based on the percentage and the sum of all values.
        """
        if abs(pct > 0.000000000001):
            absolute = int(np.rint((pct / 100.0) * np.sum(all_vals)))
            return "{:d}".format(absolute)
        else:
            return ""

    # ------------------------------------------------

    # Create a list of dictionaries that contain the results for each phase. The first element contains the info of
    # ALL phases.
    direct_matches_dicts = [hgdatautils.get_wins_direct_matches_dict(df, team1, team2, phase=opt)
                            for opt in ('', 'regular', 'repesca', 'cuartos', 'semis', 'finales')]

    # Create a np.Array 2D. Keep only home and away results.
    #   NOTE: here we order in a different than the dictionary since when it's plotted
    #         we want to display team1 wins and team2 wins next to each other separated
    #         by the draws.
    direct_matches = np.array([(d[team1], d['Empate'], d[team2]) for d in direct_matches_dicts])

    # Create a new ndarray for granulated wins
    #     direct_matches is a m x n x o ndarray
    #         m: 6 - all results + number of phases, i.e., All, Regular, Classif, Quarters, Semis, Finals
    #         n: 3 - Each team: team1, team2, draw
    #         o: 2 - wins at home, wins away, NOTE: should I include the total here to avoid computing it later?
    granulated_wins = np.array([direct_matches[1:, 0, 2],
                                direct_matches[1:, 1, 2],
                                direct_matches[1:, 2, 2]])

    results_colors = hgpltutils.get_results_colors_for_pie(team1, team2)
    home_away_colors = hgpltutils.get_home_away_wins_colors_for_pie(team1, team2)
    phases_colors = plt.cm.get_cmap('Greys', 7)

    fig, ax2 = plt.subplots()
    fig.set_size_inches(5, 5)
    # fig.tight_layout()
    # fig.suptitle(f'{team1} vs {team2}\nResultados de {df.shape[0]} duelos en torneos cortos', fontsize=title_fontsize)

    #
    #
    #
    wedge_size = 0.3
    text_size_outer = 20
    text_size_inner = 18
    text_pos_outer = 0.85
    text_pos_inner = 0.75

    # ------------------------------------------------
    #  Plot 2 - Total wins + phases
    # ------------------------------------------------
    ax2.set_title(f'Fases', fontdict={'fontsize': title_fontsize - 4})

    print(direct_matches[0])
    print(direct_matches[0, :, 2])
    print(direct_matches[0, :, 2].flatten())
    patches3, texts3, autotexts3 = ax2.pie(direct_matches[0, :, 2].flatten(), labels=(team1, 'Empates', team2),
                                           autopct=lambda pct: func(pct, direct_matches[0, :, 2].flatten()),
                                           radius=1, wedgeprops=dict(width=wedge_size, edgecolor='w'),
                                           colors=results_colors, startangle=90, pctdistance=text_pos_outer,
                                           textprops=dict(color="w", fontsize=text_size_outer))

    # patches4, texts4, autotexts4 = ax2.pie(granulated_wins.flatten(),
    #                                        # labels=('R', 'C', 'Q', 'S', 'F')*granulated_wins.shape[0],
    #                                        autopct=lambda pct: func(pct, granulated_wins),
    #                                        radius=1 - wedge_size, wedgeprops=dict(width=wedge_size, edgecolor='w'),
    #                                        colors=phases_colors(range(2, 8)), startangle=90, pctdistance=text_pos_inner,
    #                                        textprops=dict(color="w", fontsize=text_size_inner))

    for wedge3 in patches3:
        wedge3.set_alpha(0.8)

    # for wedge in patches4:
    #     wedge.set_alpha(0.75)
