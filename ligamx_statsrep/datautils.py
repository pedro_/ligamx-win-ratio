import pandas as pd
import logging


def get_df_phase_filtered(df, phase=''):
    """
    Return a filtered version of the DataFrame df according to phase.
    Take a look at the Notes for a warning.

    Args:
        df (pandas.DataFrame):
        phase (str, opt): Only data having this phase will be selected from the DataFrame.
                          Valid values: default '', 'regular', 'repesca', 'cuartos', 'semis', 'finales', 'liguilla',
                          'directa'.

    Returns:
        pandas.DataFrame: A possible filtered version of df.

    Notes:
        - WARNING. This function does not create a copy of the DataFrame df, therefore, any changes made later to the
          returned DataFrame will be also made to the original DataFrame df.
        - About phases:
          '': default, all games from all phases.
          'regular': all games from regular tournament.
          'repesca': games to classify to liguilla.
          'cuartos': liguilla quarters.
          'semis': liguilla semifinals.
          'finales': liguilla finals.
          'liguilla': only games from liguilla.
          'directa': direct elimination games (repesca and liguilla).
    """
    filtered_df = df

    if phase == 'regular':
        filtered_df = df[df.phase_int == 0]
    elif phase == 'repesca':
        filtered_df = df[df.phase_int == -1]
    elif phase == 'cuartos':
        filtered_df = df[df.phase_int == 4]
    elif phase == 'semis':
        filtered_df = df[df.phase_int == 2]
    elif phase == 'finales':
        filtered_df = df[df.phase_int == 1]
    elif phase == 'liguilla':
        filtered_df = df[df.phase_int > 0]
    elif phase == 'directa':
        filtered_df = df[df.phase_int != 0]

    return filtered_df
    # -----------------------------------------------------------------------------------------------------------------


def get_direct_matches_df(df, team1, team2, year=None, phase=''):
    """
    Return a DataFrame with the results involving team1 and team2 from the given phase. Filtered with 'contains' method.

    Args:
        df
        team1: str
        team2: str
        year (int, opt):
        phase (str, opt): Only data having this phase will be selected from the DataFrame.
                          Valid values: default '', 'regular', 'repesca', 'cuartos', 'semis', 'finales', 'liguilla',
                          'directa'.

    Returns:
        pandas.DataFrame
    """
    teams = ((df.team1.str.contains(team1)) & (df.team2.str.contains(team2))) \
            | ((df.team1.str.contains(team2)) & (df.team2.str.contains(team1)))

    direct_results_df = df[teams].copy()

    # direct_results_df['year'] = df.date.str[:4].astype(int)
    # direct_results_df.drop(direct_results_df[direct_results_df.year < year].index, inplace=True)

    if year is not None:
        try:
            direct_results_df.drop(
                direct_results_df[direct_results_df.date.dt.year < pd.to_datetime(f'2005, 12, 31').year].index,
                inplace=True)
        except:
            logging.warning(f"Couldn't drop rows with year lower than {year}. Check if it's valid.")

    direct_results_df = get_df_phase_filtered(direct_results_df, phase)

    return direct_results_df
    # -----------------------------------------------------------------------------------------------------------------


def get_wins_direct_matches_dict(df, team1, team2, phase='regular'):
    """
    Compute the total of victories, draws and defeats for each team in direct matches.

    Args:
        df: pandas.DataFrame - Direct matches data
        team1 (str):
        team2 (str):
        phase (str, opt): Only data having this phase will be selected from the DataFrame.
                          Valid values: 'regular' (default), 'repesca', 'cuartos', 'semis', 'finales', 'liguilla',
                          'directa'.

    Returns:
        dict - Containing {team1: (victories), team2: (victories), 'Empates': (draws)}
    """
    # First filter according to the option
    # If not one of the options below, assume matches from all phases were requested.
    df = get_df_phase_filtered(df, phase)

    team1_v_team2_df = df[df.team1.str.contains(team1) & df.team2.str.contains(team2)]
    team2_v_team1_df = df[df.team1.str.contains(team2) & df.team2.str.contains(team1)]

    with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        # print('-------------------------------------------------------')
        # print(team1_v_team2_df)
        pass

    # DRAWS
    draws_team1_home = 0
    draws_team2_home = 0
    # Filter draws on team1's home
    draws_team1_home = team1_v_team2_df[team1_v_team2_df.team1_score == team1_v_team2_df.team2_score].shape[0]
    # Filter draws on team2's home
    draws_team2_home = team2_v_team1_df[team2_v_team1_df.team1_score == team2_v_team1_df.team2_score].shape[0]
    # Total draws between team1 and team2
    draws = draws_team1_home + draws_team2_home

    # WINS T1 over T2
    wins_team1_home = 0
    wins_team2_home = 0

    # Filter team1's victories at home
    wins_team1_home = team1_v_team2_df[team1_v_team2_df.team1_score > team1_v_team2_df.team2_score].shape[0]
    # Filter team1's victories away
    wins_team1_away = team2_v_team1_df[team2_v_team1_df.team1_score < team2_v_team1_df.team2_score].shape[0]
    # Total victories of team1 over team2
    total_wins_team1 = wins_team1_home + wins_team1_away

    # WINS T2 over T1
    wins_team2_home = 0
    wins_team2_away = 0
    # Filter team2's victories at home
    wins_team2_home = team2_v_team1_df[team2_v_team1_df.team1_score > team2_v_team1_df.team2_score].shape[0]
    # Filter team2's victories away
    wins_team2_away = team1_v_team2_df[team1_v_team2_df.team1_score < team1_v_team2_df.team2_score].shape[0]
    # Total victories of team2 over team1
    total_wins_team2 = wins_team2_home + wins_team2_away

    # # DRAWS
    # draws_team1_home = 0
    # draws_team2_home = 0
    # # Filter draws on team1's home
    # draws_team1_home = team1_home_df[df[team1_home].team1_score == df[team1_home].team2_score].shape[0]
    # # Filter draws on team2's home
    # draws_team2_home = team2_home_df[df[team2_home].team1_score == df[team2_home].team2_score].shape[0]
    # # Total draws between team1 and team2
    # draws = draws_team1_home + draws_team2_home
    #
    # # WINS T1 over T2
    # wins_team1_home = 0
    # wins_team2_home = 0
    #
    # # Filter team1's victories at home
    # wins_team1_home = team1_home_df[df[team1_home].team1_score > df[team1_home].team2_score].shape[0]
    # # Filter team1's victories away
    # wins_team1_away = team2_home_df[df[team2_home].team1_score < df[team2_home].team2_score].shape[0]
    # # Total victories of team1 over team2
    # total_wins_team1 = wins_team1_home + wins_team1_away
    #
    # # WINS T2 over T1
    # wins_team2_home = 0
    # wins_team2_away = 0
    # # Filter team2's victories at home
    # wins_team2_home = team2_home_df[df[team2_home].team1_score > df[team2_home].team2_score].shape[0]
    # # Filter team2's victories away
    # wins_team2_away = team1_home_df[df[team1_home].team1_score < df[team1_home].team2_score].shape[0]
    # # Total victories of team2 over team1
    # total_wins_team2 = wins_team2_home + wins_team2_away

    return {team1: (wins_team1_home, wins_team1_away, total_wins_team1),
            team2: (wins_team2_home, wins_team2_away, total_wins_team2),
            'Empate': (draws_team1_home, draws_team2_home, draws)}
    # -----------------------------------------------------------------------------------------------------------------


