import pandas as pd
from ligamx_statsrep import barplotter as hgbarplotter
from ligamx_statsrep import datautils as hgdatautils
from ligamx_statsrep import pieplotter as hgpieplotter


# TODO: temp
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from ligamx_statsrep import plotutils as hgplotutils
# -----------------



def set_opts_full_df():
    pd.set_option('display.max_rows', None)
    pd.set_option('display.max_columns', None)
    pd.set_option('display.width', None)
    pd.set_option('display.max_colwidth', -1)


def reset_opts_full_df():
    pd.reset_option('display')


def full_df_opts_context():
    return pd.option_context('display.max_rows', None,
                             'display.max_columns', None,
                             'display.width', None,
                             'display.max_colwidth', -1)


def vs_wins_bar(df, team1, team2, phase='all', save_fig=False):
    """
    Generate a bar plot depicting the results between team1 and team2 split by home and away, and grouped by phase.

    Args:
        df (pd.DataFrame):
        team1 (str):
        team2 (str):
        phase (str, opt):
        save_fig (bool, opt):

    Returns:
        None

    """
    vs_df = hgdatautils.get_direct_matches_df(df, team1, team2)

    # with full_df_opts_context():
    #     print(vs_df[vs_df.season.str.contains('20??')])

    # hgpieplotter.pie_plot_wins_home_away(vs_df, team1, team2, save_fig=True, phase='regular')
    # hgpieplotter.pie_plot_wins_home_away(vs_df, team1, team2, save_fig=True, phase='directa')
    # hgpieplotter.pie_plot_wins_home_away(vs_df, team1, team2, save_fig=True, phase='liguilla')
    hgbarplotter.plot_wins(vs_df, team1, team2, save_fig=save_fig, phase=phase)

    # -----------------------------------------------------------------------------------------------------------------


def get_cdf_xy(data):
    """
    Return the np.arrays to plot a CDF or ECDF.

    Args:
        data (list-like): Data to sort and be returned as

    Returns:
        (np.array, np.array)
        2-tuple. Each element is an np.array. These values are ready to be passed to a plotting API.
    """
    x = np.sort(data)
    n = len(x)
    y = np.arange(1, n+1) / n

    return x, y


def historical_statistics(df, *args):
    """
    Return a list of DataFrames, each containing the historical summary statistics of each team passed in *args.
    This data is almost ready to plot since it adds the index season_nom. This can be used to plot as a time series
    but changing the labels to season.
    Warning: The season column is different. It has a shorter nomenclature more suitable for plotting purposes.

    Args:
        df (pd.DataFrame):  DF to extract the data from
        args (str): This is a list of teams to get their historical statistics

    Returns:
        list of pd.DataFrames
        Returns a list containing a reference to a DF summarizing the historical statistic of each team passed in *args

    """
    # Create a new nomenclature for the season. Just keep the first letter and the year.
    # This will be used when sorting and plotting as labels
    df['season'] = df.season.str.replace(r'[a-z ]', '', regex=True)
    # I1996-V1997-I1997-...-I2001-V2002 - A2002-C2003-A2003-C2004-A2004-...-C2009-A2009-B2010-A2010-...-C2020*-A2020-C2021

    # How to group and sort the seasons
    #   Replace the letter/term by a number, to have the following yyyy-x, where x is 1 or 2 according to:
    #      V=C=B=1
    #      I=A=2
    # TODO: is there a way to do this in a single regex?
    df['season_nom'] = df.season.str.replace(r'^([IA])[a-z ]*(\d\d\d\d)', r'\g<2>-2', regex=True)
    df['season_nom'] = df.season_nom.str.replace(r'^([VCB])[a-z ]*(\d\d\d\d)', r'\g<2>-1', regex=True)

    # Create a two new columns that will contain the points made by all teams in each match.
    # How it works: compute boolean vectors, and multipy each of them by the points won, a victory is 3 and a draw is 1.
    #               After this, sum them, since a boolean vector will contain many zeroes, adding up victories and draws
    #               will fill out the proper results.
    #  Note: This might be redundant info, but I think it will ease some operations when computing the total points by
    #  season
    df['team1_points'] = (df.team1_score > df.team2_score) * 3 + (df.team1_score == df.team2_score)
    df['team2_points'] = (df.team2_score > df.team1_score) * 3 + (df.team1_score == df.team2_score)

    # Declare the empty list that will be returned with DataFrames
    res = []

    for team in args:
        print(team)
        with full_df_opts_context():
            # Get two DFs, one with results at home and the other as away
            team_home_res = df[(df.phase_int == 0) & (df.team1 == team)]
            team_away_res = df[(df.phase_int == 0) & (df.team2 == team)]

            # Compute general statistics: points in each season, goals made, goals received and number of matches played.
            # This will serve as a base to later include the home and away statistics
            team_historical_stats = team_home_res\
                                    .groupby(by=['season_nom', 'season'], sort=False) \
                                    .agg(team_points=pd.NamedAgg(column='team1_points', aggfunc='sum'),
                                         team_goals_scored=pd.NamedAgg(column='team1_score', aggfunc=sum),
                                         team_goals_received=pd.NamedAgg(column='team2_score', aggfunc=sum),
                                         team_matches=pd.NamedAgg(column='team1_points', aggfunc='count'))\
                                + team_away_res\
                                    .groupby(by=['season_nom', 'season'], sort=False)\
                                    .agg(team_points=pd.NamedAgg(column='team2_points', aggfunc='sum'),
                                         team_goals_scored=pd.NamedAgg(column='team2_score', aggfunc=sum),
                                         team_goals_received=pd.NamedAgg(column='team1_score', aggfunc=sum),
                                         team_matches=pd.NamedAgg(column='team2_points', aggfunc='count'))

            # Compute home and away statistics: points in each season, goals made and goals received.
            team_historical_stats_home = team_home_res\
                                     .groupby(by=['season_nom', 'season'], sort=False)\
                                     .agg(team_home_points=pd.NamedAgg(column='team1_points', aggfunc='sum'),
                                          team_goals_scored_home=pd.NamedAgg(column='team1_score', aggfunc=sum),
                                          team_goals_received_home=pd.NamedAgg(column='team2_score', aggfunc=sum))

            team_historical_stats_away = team_away_res\
                                     .groupby(by=['season_nom', 'season'], sort=False)\
                                     .agg(team_away_points=pd.NamedAgg(column='team2_points', aggfunc='sum'),
                                          team_goals_scored_away=pd.NamedAgg(column='team2_score', aggfunc=sum),
                                          team_goals_received_away=pd.NamedAgg(column='team1_score', aggfunc=sum))

            # Now merge everything in the general table
            # TODO: Is there an easy way to do this in few lines?
            team_historical_stats = pd.merge(left=team_historical_stats, right=team_historical_stats_home,
                                             left_index=True, right_index=True)
            team_historical_stats = pd.merge(left=team_historical_stats, right=team_historical_stats_away,
                                             left_index=True, right_index=True)

            # print(team_historical_stats)

            # print(team_historical_stats_home)
            # print(team_historical_stats_away)

            # Create a new DF with season_nom as index and sort it so looks like a time series, since team_historical_points
            # is a Series with MultiIndex [season_nom, season]
            team_historical_stats = team_historical_stats.reset_index(level='season').sort_index()
            res.append(team_historical_stats)

        #     # --------------------------
        #     #  Tests
        #     # --------------------------
        #     # Test query to get team_historical_points
        #     for season, season_nom in team_historical_points.index:
        #         pts_query = team_historical_points[season, season_nom]
        #         pts_reference = team_home_res[team_home_res.season_nom.str.contains(season)].team1_points.sum() \
        #                         + team_away_res[team_away_res.season_nom.str.contains(season)].team2_points.sum()
        #
        #         # print(pts_query, pts_reference)
        #         assert pts_query == pts_reference
        #
        #         # print(team_home_res[team_home_res.season.str.contains(season)].sort_values(by='date'))
        #         # print(team_away_res[team_away_res.season.str.contains(season)].sort_values(by='date'))
        #

    return res
    # -----------------------------------------------------------------------------------------------------------------

def plot_historical_statistics(team: 'str'):
    hist_stats = historical_statistics(ligamx_results_df, team)

    fig, ax1 = plt.subplots()
    longest_season = 0  # TODO: determine if this is going to remain. This is just to have the maximum number of \
    # possible points that has a team (remember that there were some seasons that had 19 rounds/jornadas of
    # regular season, but not all teams existed when that happened. So, if we allow comparison of
    # multiple teams at once, we have to compare them using the maximum rounds they ever played.
    for team_hist_stats in hist_stats:
        # Get maximum games ever played by any of the teams
        max_team_matches = team_hist_stats.team_matches.max()
        longest_season = max_team_matches if max_team_matches > longest_season else longest_season
    
        color = hgplotutils.LIGAMX_COLORS[team]
        if int(color[1:], 16) > 0xe9e9e9ff:
            print(int(color[1:], 16), 0xe9e9e9)
            color = hgplotutils.LIGAMX_COLORS_TEXT[team]
    
    
        # _ = plt.plot(team_historical_points.index.levels[1], team_historical_points.values, color=color, linewidth=1)
        _ = ax1.plot(team_hist_stats.season, team_hist_stats.team_points, marker='o', color=color)
        _ = ax1.set_xlabel('Torneo', fontsize=14)
        _ = ax1.set_ylabel('Puntos', fontsize=14)
        _ = ax1.set_ylim([0, longest_season*3])
    
        ax2 = ax1.twinx()
        _ = ax2.plot(team_hist_stats.season, 100 * team_hist_stats.team_points / (3 * team_hist_stats.team_matches),
                    marker='o', color='orange')
        _ = ax2.set_ylabel('Porcentaje de puntos posibles', fontsize=14)
        _ = ax2.set_ylim([0, 100])
    
        # Common settings
        _ = ax1.set_xticklabels(team_hist_stats.season, rotation=60)
        ax1.grid('on')
        #plt.axis('tight')
    plt.show()



    get_prob_team1_win_away(ligamx_results_df, team1, team2)
    get_prob_team1_win_away(ligamx_results_df, team2, team1)
    samples = np.random.binomial(100, 0.1, size=100000)
    x_theor, y_theor = get_cdf_xy(samples)
    
    _ = plt.plot(x_theor, y_theor)
    
    x, y = get_cdf_xy(hist_stats[0].team_points)
    # _ = plt.twinx()
    
    sns.set()
    _ = plt.plot(x, y, marker='.', linestyle='none')
    _ = plt.xlabel('Puntos')
    _ = plt.ylabel('ECDF')
    
    plt.margins(0.1)
    plt.show()


def get_latest_wins_away(df, team1, team2):
    # Get victories of each team as away. DF

    team1_latest_wins_away = df[
        ((df.team1 == team2) & (df.team2 == team1)) &
        (df.team2_score > df.team1_score)].sort_values(by='date', ascending=False)
    team2_latest_wins_away = df[
        ((df.team1 == team1) & (df.team2 == team2)) &
        (df.team2_score > df.team1_score)].sort_values(by='date', ascending=False)

    # print(team1_latest_wins_away)
    # print()
    # print(team2_latest_wins_away)
    # -----------------------------------------------------------------------------------------------------------------


def get_prob_team1_win_away(df, team1, team2):
    team1_latest_wins_away = df[
        ((df.team1 == team2) & (df.team2 == team1)) & (
                df.team2_score > df.team1_score)].sort_values(by='date', ascending=False)

    ratio = team1_latest_wins_away.shape[0] / df[((df.team1 == team2) & (df.team2 == team1))].shape[0]
    print(ratio)
    print(team1_latest_wins_away.shape[0])
    print(df[((df.team1 == team2) & (df.team2 == team1))].shape[0])

    team1_wins_away = np.random.poisson(ratio, size=10000)
    x_theor, y_theor = get_cdf_xy(team1_wins_away)
    _ = plt.plot(x_theor, y_theor)

    sns.set()
    _ = plt.plot(x_theor, y_theor, marker='.', linestyle='none')
    _ = plt.xlabel('Puntos')
    _ = plt.ylabel('ECDF')
    plt.show()

    n_large = np.sum(team1_wins_away > 2)
    p_large = n_large / 10000
    print(f'Probability: {p_large}')


# def get_hist_win_trend(df, team1, team2):
#     df.team1


if __name__ == '__main__':
    ligamx_results_df = pd.read_csv('input/vef-ligamx-short-seasons.csv', parse_dates=['date'])

    # ------------------------------------------------------
    # Data preparation
    # ------------------------------------------------------
    # Clean the DataFrame
    #  1. First remove the stadium column which is all NaN
    #  2. Drop all NaN rows remaining
    ligamx_results_df.drop(columns='stadium', inplace=True)
    ligamx_results_df.dropna(inplace=True)

    # TODO: Improve the scraper to handle the season C2020. This season was canceled at J10, but the page
    #  www.vivoelfutbol.com filled the results with draws starting from J11. So, we have to stop getting the data after
    #  J10. Here we filter it by date. Last games from J10 were on Dom 15 de Mar de 2020
    rounds_cancelled_c2020 = ligamx_results_df[(ligamx_results_df.season == 'Clausura 2020')
                                               & (ligamx_results_df.date > pd.to_datetime('2020, 3, 15'))]

    ligamx_results_df.drop(index=rounds_cancelled_c2020.index, inplace=True)

    # Convert 'team1_score' and 'team2_score' to integers
    # TODO: I think this could be prevented if when scraping, don't include non numerical values. This happened because
    #       vivoelfutbol.com has all stages but matches not played have "Por definir" as value.
    ligamx_results_df['team1_score'] = ligamx_results_df.team1_score.astype(int)
    ligamx_results_df['team2_score'] = ligamx_results_df.team2_score.astype(int)
    # ------------------------------------------------------


    # with full_df_opts_context():
    #     print(ligamx_results_df[(ligamx_results_df.season == 'Clausura 2020') & (ligamx_results_df.date > pd.to_datetime('2020, 3, 15'))])

    team1 = 'Santos'
    team2 = 'Guadalajara'

    vs_wins_bar(ligamx_results_df, team1, team2, save_fig=True)
    
    # vs_df = hgpieplotter.get_direct_matches_df(ligamx_results_df, team1, team)
    hgpieplotter.pie_plot_wins_home_away(ligamx_results_df, team1, team2, save_fig=True, phase='regular')

