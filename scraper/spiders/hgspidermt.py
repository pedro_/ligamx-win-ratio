import scrapy
import logging
import numpy
import os
import datetime

import locale
import threading

import contextlib


class MTCalendarSpider(scrapy.Spider):
    name = "mt-calendar-spider"

    custom_settings = {
        'LOG_FILE': 'MTCalendarSpider.log',
        'LOG_LEVEL': 'DEBUG',

        # https://docs.scrapy.org/en/0.14/faq.html#does-scrapy-crawl-in-breath-first-or-depth-first-order
        #'DEPTH_PRIORITY': 1,
        #'SCHEDULER_DISK_QUEUE': 'scrapy.squeue.PickleFifoDiskQueue',
        #'SCHEDULER_MEMORY_QUEUE': 'scrapy.squeue.FifoMemoryQueue'
    }

    MT_URL = "https://www.mediotiempo.com"
    MT_CALENDAR_URL = 'https://www.mediotiempo.com/futbol/liga-mx/calendario'

    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/199-2020/quarter-finals'
    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/199-2020/final'
    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/385-2020/regular/3'
    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/385-2016/quarter-finals'
    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/385-2016/semi-finals'
    # MT_CALENDAR_URL_DBG = 'https://www.mediotiempo.com/futbol/liga-mx/calendario/385-2016/final'

    # Phases will be expressed also as a number:
    #    0: regular
    #   -1: classification(repechaje, if any), -2 classification 2 (repechaje 2, if any), ...
    #    1: final, 2:semifinal, 4:quarters, 8:eights, 16:sixteenths, 32:thirty-seconds, ...
    PHASES = {'Regular': 0,
              'Repechaje': -1,
              'Liguilla Cuartos De Final': 4, 'Cuartos De Final': 4,
              'Semi-Finals': 2, 'Semifinal': 2,
              'Final': 1}

    # There are only 4 out of 12 different months. Is there a way to include only the ones different and leave the other
    # unchanged? lambda function?
    MONTH_FROM_SPA_TO_ENG = {'ENE': 'JAN', 'FEB': 'FEB', 'MAR': 'MAR', 'ABR': 'APR', 'MAY': 'MAY', 'JUN': 'JUN',
                             'JUL': 'JUL', 'AGO': 'AUG', 'SEP': 'SEP', 'OCT': 'OCT', 'NOV': 'NOV', 'DIC': 'DEC'}

    LIGAMX_FILE = "ligamx-short-seasons.csv"

    LIGAMX_FILE_HEADER = "SEASON,DATE,TIME,DATETIME,"\
                         "PHASE,PHASE_INT,ROUND,TEAM1,TEAM1_SCORE,TEAM2,TEAM2_SCORE,STADIUM\n"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.short_seasons_links = []  # List to keep links to the short seasons
        # self.cur_season = None  # Current season being parsed. str
        # self.cur_phase = None  # Current phase being parsed. int:

        if os.path.exists(MTCalendarSpider.LIGAMX_FILE):
            os.remove(MTCalendarSpider.LIGAMX_FILE)

    def start_requests(self):
        # Create the CSV file and add the header
        with open(MTCalendarSpider.LIGAMX_FILE, 'a') as f:
            f.write(MTCalendarSpider.LIGAMX_FILE_HEADER)

        # DEBUGGING
        # yield scrapy.Request(url=MTCalendarSpider.MT_CALENDAR_URL_DBG, callback=self.parse_matches)
        # COMPLETE
        yield scrapy.Request(url=MTCalendarSpider.MT_CALENDAR_URL, callback=self.parse_main_calendar_page)

    def parse_main_calendar_page(self, response):
        """

        :param response:
        :return:
        """
        css_season_selector = 'div.apertura-select > div.dropdown-content > a::attr(href)'
        # .select - seasons > div: nth - child(1) > div:nth - child(2) > a: nth - child(1)
        xpath_season_selector = '//div[@class="apertura-select"]//div[@class="dropdown-content"]/a/@href'

        season_rel_urls = response.xpath(xpath_season_selector).getall()

        for url in season_rel_urls:
            if url.find("temporada") >= 0:
                # If we found "temporada" the it means that we have all short seasons
                break
            self.short_seasons_links.append(MTCalendarSpider.MT_URL + url)

        for season_link in self.short_seasons_links:
            yield response.follow(url=season_link, callback=self.parse_season_page)

    def parse_season_page(self, response):
        """
        Callback method
        Parse the season page contents to extract the URLs of the phases: Regular, Repechaje, Cuartos,
        Semifinales, Final
        :param response: response object from Scrapy
        :return: yields a Scrapy.Response
        """
        xpath_phases_selector = '//div[@class="fase-select"]//div[@class="dropdown-content"]/a/@href'
        phases_rel_urls = response.xpath(xpath_phases_selector).getall()
        phases_urls = []

        for url in phases_rel_urls:
            phases_urls.append(MTCalendarSpider.MT_URL + url)

        for url in phases_urls:
            yield response.follow(url=url, callback=self.parse_phase_page)

    def parse_phase_page(self, response):
        """
        Callback method
        Parse the phase page contents to extract the URLs of the phases: Regular, Repechaje, Cuartos,
        :param response: response object from Scrapy
        :return:
        """

        # Get all the Jornada links and parse each page. For other phases, just pass the original link.
        if response.url.find("regular") >= 0:
            xpath_matches_selector = '//div[@class="jornada-select"]//div[@class="dropdown-content"]/a/@href'
            matches_rel_urls = response.xpath(xpath_matches_selector).getall()
            for url in matches_rel_urls:
                yield response.follow(url=MTCalendarSpider.MT_URL + url, callback=self.parse_fixture)
        else:
            # This doesn't work for som reason. Call directly the parsing function instead of using it as a callback
            # yield response.follow(url=response.url, callback=self.parse_matches)
            self.parse_fixture(response)

    def parse_fixture(self, response):
        """

        :param response:
        :return:
        """

        # List to store the information that will be written to the file
        csv_lines = []

        # Selectors to extract season and phase
        season_selector = './/div[@class="apertura-select"]//button/text()'
        phase_selector = './/div[@class="fase-select"]//button/text()'

        # Extract the season and the phase of the results we are about to get
        season = response.xpath(season_selector).get()
        if season:
            season = season.strip()
        else:
            print(response)

        phase = response.xpath(phase_selector).get()
        cur_phase_int = None
        if phase:
            phase = phase.strip()
            cur_phase_int = MTCalendarSpider.PHASES[phase]
        else:
            print(response)


        # --------------------------------------------------
        #  Structure of the data in the page
        # --------------------------------------------------
        # - div class=ctr-stadistics-header__body
        #   - div class="ctr-stadistics-header__header"
        #     - div class="ctr-stadistics-header__round"
        #       - span
        #   - table class=ctr-stadistics-header__table"
        #     - tr class="ctr-stadistics-header__row ctr-stadistics-header__row--head"
        #     - tr class="ctr-stadistics-header__row ctr-stadistics-header__row--golden"
        #     - tr class="ctr-stadistics-header__row"
        #   - div class="ctr-stadistics-header__header"
        #     - div class="ctr-stadistics-header__round"
        #       - span
        #   - table class="ctr-stadistics-header__table"
        #   ...
        #
        results_body_sel = '//div[@class="ctr-stadistics-header__body"]'
        results_rounds_sel = './/div[@class="ctr-stadistics-header__round"]'
        results_tables_sel = './/table[@class="ctr-stadistics-header__table"]'

        # Extract the round we are about to parse. It can be only one, like a Jornada. Or it can be Ida y Vuelta when
        # we are in Liguilla
        results_body = response.xpath(results_body_sel)
        results_rounds = results_body.xpath(results_rounds_sel)
        results_tables = results_body.xpath(results_tables_sel)
        #print("Number of tables: ", len(results_tables))
        #print("Number of rounds: ", len(results_rounds))

        # NOTE: this will select one header in each table.
        results_rows_selector = './/tr[contains(@class, "ctr-stadistics-header__row")]'

        # More selectors to extract the information of each row
        date_selector = './/div[@class="ctr-stadistics-header__date"]//span/text()'
        time_selector = './/div[@class="ctr-stadistics-header__time"]//span/text()'
        home_selector = './/div[@class="ctr-stadistics-header__first-team"]//span/text()'
        away_selector = './/div[@class="ctr-stadistics-header__second-team"]//span/text()'
        result_selector = './/div[@class="ctr-stadistics-header__result-team"]//span/text()'
        stadium_selector = './/td[contains(@class, "ctr-stadistics-header__cell--place")]//span/text()'

        # Extract the values for each table corresponding to the round. The number of tables is the number of rounds.
        # But ensure that we have at least one table.
        if len(results_tables) == len(results_rounds):
            table_index = 0
            for res_round in results_rounds:
                match_round = res_round.xpath(".//span/text()").get()
                if match_round:
                    match_round = match_round.strip()

                table = results_tables[table_index]

                # Get all the rows from the current table
                results_rows = table.xpath(results_rows_selector)

                last_date = None
                last_full_date = None

                # Extract the information from each row.
                for row in results_rows:
                    match_tmp_date = row.xpath(date_selector).get()
                    match_time = row.xpath(time_selector).get()
                    team1 = row.xpath(home_selector).get()
                    team2 = row.xpath(away_selector).get()
                    match_result = row.xpath(result_selector).get()
                    match_stadium = row.xpath(stadium_selector).get()
                    team1_score = numpy.NaN
                    team2_score = numpy.NaN

                    # NOTE: AT LEAST ONE header row is always selected since it contains the
                    #          @class="ctr-stadistics-header__row".
                    #       but doesn't contain relevant information. Need to skip the row.
                    #       The header row is: FECHA/HORA, PARTIDO, ESTADIO, ESTADO
                    # If most of the extracted information is None, it means it's a header row. In the logic below, find at
                    # least one of those values being not None. If so, we process it. Otherwise, go to the next row
                    if not (match_tmp_date or match_time or team1 or team2):
                        continue

                    #
                    # Get the math date
                    #
                    # Only row--golden class contain a date. If it's a regular row, use the previous date
                    if match_tmp_date is not None:
                        # Extract the year. The year is always the last word
                        year = season.split()[-1]
                        # Extract the month, which has to be translated to english. It's always in the format dd MMM, so,
                        # month starts at index 3 and ends at 5
                        month = MTCalendarSpider.MONTH_FROM_SPA_TO_ENG[match_tmp_date[3:6]]
                        day = match_tmp_date[:2]
                        last_full_date = f"{day} {month} {year}"
                        last_date = match_tmp_date

                    match_date = last_date
                    # Create a datetime object
                    # https://docs.python.org/3.7/library/time.html#time.strptime
                    full_datetime = f"{last_full_date} {match_time}"
                    match_datetime = datetime.datetime.strptime(full_datetime, "%d %b %Y %H:%M")


                    #
                    # Get the score for each team
                    #
                    # This is ugly, but need to clean up the result. First split the result
                    match_home_score_str, match_away_score_str = match_result.split('-')

                    # Remove the spaces
                    match_home_score_str = match_home_score_str.strip()
                    match_away_score_str = match_away_score_str.strip()

                    # if the results are not blank (empty string), assign the correct value (int)
                    if match_home_score_str:
                        team1_score = int(match_home_score_str)

                    if match_away_score_str:
                        team2_score = int(match_away_score_str)

                    #
                    # Build the desired CSV structure
                    #
                    line = f"{season.strip()},{match_date.strip()},{match_time.strip()},{match_datetime},"\
                           f"{phase.strip()},{cur_phase_int},{match_round.strip()},"\
                           f"{team1.strip()},{team1_score},"\
                           f"{team2.strip()},{team2_score},{match_stadium.strip()}\n"
                    csv_lines.append(line)
                    # print(line)

                # Continue with the next table
                table_index += 1

        # Append the information to our CSV file
        with open(MTCalendarSpider.LIGAMX_FILE, "a") as f:
            f.writelines(csv_lines)


