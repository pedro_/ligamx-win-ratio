import logging
import os
import scrapy
import requests

# Configure logging. Use only the stdout
# logging.basicConfig(format= "%(levelname)s: %(message)s", level=logging.INFO)
#


url_mt_main = 'https://www.mediotiempo.com/'

xpath_logos = '//img[contains(@alt, "Escudos Liga MX")]/@data-src'

ARTIFACTS_LOGOS_REL_PATH = './artifacts/logos'


class MTLigaMXLogosSpider(scrapy.Spider):
    name = "mt-logos-spider"

    def start_requests(self):
        urls = [url_mt_main]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.get_logos)

    def get_logos(self, response):
        self._create_dir(ARTIFACTS_LOGOS_REL_PATH)

        logos_urls = response.xpath(xpath_logos).extract()
        logging.info(logos_urls)

        for logo_url in logos_urls:
            # response.follow()

            # Download logos
            image = requests.get(logo_url).content

            # Get the name of the file from the url
            img_filename = logo_url[logo_url.rfind('/') + 1:]
            logging.info("Downloading: %s", img_filename)

            img_rel_path = os.path.join(ARTIFACTS_LOGOS_REL_PATH, img_filename)
            with open(img_rel_path, "wb") as f:
                f.write(image)

    @staticmethod
    def _create_dir(path):
        """
        Try to create the relative path declared by 'path'. It supports recursive path.

        Args:
            path (str): Path to create. Separator must be in UNIX format, i.e., '/'

        Returns:
            None

        """
        dirs = path.split('/')

        print(dirs)

        for i, dir in enumerate(dirs):
            reconstructed = '/'.join(dirs[:i + 1])

            print(reconstructed)

            if not (os.path.exists(reconstructed)):
                try:
                    os.mkdir(reconstructed, 0o755)
                    print("  Created {}".format(reconstructed))
                except:
                    raise Exception("Couldn't create {}.".format(reconstructed))
            else:
                if not (os.path.isdir(reconstructed)):
                    raise Exception(
                        "{} exists and is not a directory. Rename it or delete it and execute the script again".format(
                            reconstructed))
