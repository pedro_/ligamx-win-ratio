import os
from pathlib import Path

# TODO: Need to use something like the `setuptools` module. With that I could set environment variables
LOGOS_DIR = Path(__file__).parent.parent.joinpath('scraper', 'artifacts', 'logos')

LIGAMX_COLORS = {'America': '#ffec00ff',  # '#002c54'
                 'Atlas': '#ed161f',  # '#1e191a'
                 'Atletico SL': '#a93425',  # '#f8f9fa', '#282c5c'
                 'Cruz Azul': '#2e3192',
                 'Guadalajara': '#cd1731',  # '#02172f', '#ffffff'
                 'Juarez': '#1bcc32',  # '#191716', ''
                 'Leon': '#048741',
                 'Monterrey': '#1a2741',
                 'None': '#717171',
                 'Pachuca': '#222b52',
                 'Queretaro': '#f0f0f0ff', # '#211915',
                 'San Luis': '#e9ab18ff',  # '#002b97'
                 'Santos': '#00833eff',
                 'Tigres': '#e9ae42',  # '#275ea7'
                 'Toluca': '#b1182e',
                 'UNAM': '#1a2855',  # '#ad975a'
                 }

LIGAMX_COLORS_TEXT = {'America': '#002c54', # '#182e64',
                      'Atlas': '#1e191a',
                      'Atletico SL': '#a93425',  # '#f8f9fa', '#282c5c'
                      'Cruz Azul': '#ffffff',
                      'Guadalajara': '#02172f',  # '#ffffff'
                      'Juarez': '#000000',  # '#191716', ''
                      'Leon': '#fadd01', # '#ffffff'
                      'Monterrey': '#ffffff',
                      'None': '#ffffff',
                      'Pachuca': '#fefefe',
                      'Queretaro': '#0056b3ff', # '#211915'
                      'San Luis': '#e9ab18',  # '#002b97'
                      'Santos': '#ffffffff',
                      'Tigres': '#275ea7',
                      'Toluca': '#ffffff',
                      'UNAM': '#ad975a',
                      }

LOGOS_DICT = {'America': 'escudos-liga-mx-america-1.png',
              'Atlas': 'escudos-liga-mx-atlas-1.png',
              'Atletico SL': 'escudos-liga-mx-atletico-san.png',
              'Cruz Azul': 'escudos-liga-mx-cruz-azul-1.png',
              'Juarez': 'escudos-liga-mx-fc-juarez.png',
              'Guadalajara': 'escudos-liga-mx-guadalajara-1.png',
              'Leon': 'escudos-liga-mx-leon-x.png',
              'Mazatlan': 'escudos-liga-mx-mazatlan-fc.png',
              'Monterrey': 'escudos-liga-mx-monterrey-6.png',
              'Necaxa': 'escudos-liga-mx-necaxa-1.png',
              'Pachuca': 'escudos-liga-mx-pachuca-3.png',
              'Puebla': 'escudos-liga-mx-puebla-3.png',
              'Queretaro': 'escudos-liga-mx-queretaro-1.png',
              'Santos': 'escudos-liga-mx-santos-3.png',
              'Tigres': 'escudos-liga-mx-tigres-6.png',
              'Toluca': 'escudos-liga-mx-toluca-1.png',
              'UNAM': 'escudos-liga-mx-pumas-1.png',
              'Xolos': 'escudos-liga-mx-xolos-1.png',
              }

LIGAMX_NAMES = {'America': 'América',
              'Atlas': 'Atlas',
              'Atletico SL': 'Atl. San Luis',
              'Cruz Azul': 'Cruz Azul',
              'Juarez': 'Juárez',
              'Guadalajara': 'Chivas',
              'Leon': 'León',
              'Mazatlan': 'Mazatlán',
              'Monterrey': 'Monterrey',
              'Necaxa': 'Necaxa',
              'Pachuca': 'Pachuca',
              'Puebla': 'Puebla',
              'Queretaro': 'Querétaro',
              'Santos': 'Santos',
              'Tigres': 'Tigres',
              'Toluca': 'Toluca',
              'UNAM': 'Pumas',
              'Xolos': 'Xolos',
              }


def get_team_color(team):
    if team not in LIGAMX_COLORS.keys():
        print(' Pending to implement team color.')
        return '#ff00ffff'
    else:
        return LIGAMX_COLORS[team]


def get_team_text_color(team):
    if team not in LIGAMX_COLORS_TEXT.keys():
        print(' Pending to implement team text color.')
        return '#e5e5e5ff'
    else:
        return LIGAMX_COLORS_TEXT[team]
#
# Pastel tiny palettes
# https://www.canva.com/learn/30-examples-pastel-colors/
#
# '#dc7684' # Pale violet
# '#e4ca99' # Burly wood
# '#eaf2f4' # Light steel blue
# '#2d7f9d' # Steel blue
# '#a4c9d7' # Light steel blue
#
# '#fcbc78' # Light salmon
# '#f6a24e' # Sandy brown
# '#e6cfc5' # Tan
# '#a63d11' # Saddle brown
# '#cba593' # Tan
#
# '#e97140' # Chocolate
# '#ed8e63' # Sandy brown
# '#f6f0ed' # Beige
# '#6a6767' # Dim gray
# '#a9b8b2' # Dark sea green


def create_dir(path):
    """
    Try to create the relative path declared by 'path'. It supports recursive path.

    Args:
        path (str): Path to create. Separator must be in UNIX format, i.e., '/'

    Returns:
        None

    """
    if not (os.path.exists(path)):
        dirs = path.split('/')

        print(dirs)

        for i, dir in enumerate(dirs):
            reconstructed = '/'.join(dirs[:i + 1])

            print(reconstructed)

            if not (os.path.exists(reconstructed)):
                try:
                    os.mkdir(reconstructed, 0o755)
                    print("  Created {}".format(reconstructed))
                except:
                    raise Exception("Couldn't create {}.".format(reconstructed))
            else:
                if not (os.path.isdir(reconstructed)):
                    raise Exception(
                        "{} exists and is not a directory. Rename it or delete it and execute the script again".format(
                            reconstructed))


def get_phase_text_for_title(phase):
    if phase == '' or phase == 'all':
        return 'todas las fases'
    elif phase == 'regular':
        return 'fase regular'
    elif phase == 'repesca':
        return 'fase de repesca'
    elif phase == 'cuartos':
        return 'fase de cuartos de final'
    elif phase == 'semis':
        return 'fase de semifinales'
    elif phase == 'finales':
        return 'finales'
    elif phase == 'directa':
        return 'eliminación directa (repesca y liguilla)'
    elif phase == 'liguilla':
        return 'liguillas'


def get_results_colors_for_pie(team1, team2):
    """
    Return an list of str representing hex colors. Used when plotting total wins of each team and draws.

    Args:
        team1:
        team2:

    Returns:
        3-list of str: Each str represents a hex color.

    """
    return [LIGAMX_COLORS[team]
            if team in LIGAMX_COLORS.keys() else LIGAMX_COLORS['None']
            for team in (team1, 'None', team2)]


def get_home_away_wins_colors_for_pie(team1, team2):
    """
    Return a list of str representing hex colors. Used when plotting Home-Away wins.

    Args:
        team1 (str): Name of team 1. Must match a nme in the LIGAMX_COLORS dictionary.
        team2 (str): Name of team 2. Must match a nme in the LIGAMX_COLORS dictionary.

    Returns:
        6-list of str: Each str represents an hex color.

    Notes:
        This is a problem because the order of team2 wins is the oposite to TEAM1 and DRAWS. Could either
        rearrange the data or change the order of the colors. Change the order of the colors.
    """
    return [LIGAMX_COLORS[team1], LIGAMX_COLORS[team2],
            LIGAMX_COLORS[team1], LIGAMX_COLORS[team2],
            LIGAMX_COLORS[team2], LIGAMX_COLORS[team1]]
    # return list([(LIGAMX_COLORS[team1], LIGAMX_COLORS[team2])
    #             if i < 2 else (LIGAMX_COLORS[team2], LIGAMX_COLORS[team1])
    #             for i in range(3)])
