# LigaMX stats
Create plots displaying victories and losses between two teams.


# TODO list
## New list
- Build a static frontend.
- Build a back-end using FastAPI. Use some data points to serve JSON.
  - (FastAPI)[https://fastapi.tiangolo.com/]
- Transform the data into a Postgres database.
   - (ORM SQLModel)[https://sqlmodel.tiangolo.com/]
   - (FastAPI and SQL tutorial)[https://fastapi.tiangolo.com/tutorial/sql-databases/]

## Previous list
- [FEAT] Add options to the scraper sub-project to execute it from command line
  - Among the options: full-multi, full-single, latest, logos
  - Move automatically the results to the proj/input folder?
- [FEAT] Finish implementation of pie charts for liguilla