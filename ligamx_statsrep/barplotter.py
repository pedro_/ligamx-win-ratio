import os
import numpy as np
import pandas as pd
import matplotlib as mplib
import matplotlib.pyplot as plt
from ligamx_statsrep import datautils as hgdatautils
from ligamx_statsrep import plotutils as hgpltutils
import matplotlib.offsetbox

HOME_AWAY_PLOTS_REL_DIR = 'artifacts/plots/home_away_wins'


def hg_ligamx_bar_plot(teams_dict, title='', title_fontsize=20):
    """

    Args:
        teams_dict: dict
        title (optional): str
        title_size (optional): int

        kwargs (optional):

    Returns
        tuple of objects: (matplotlib.figure.Figure, matplotlib.axes.Axes)
    """

    team_colors = [hgpltutils.LIGAMX_COLORS[team]
                   if team in hgpltutils.LIGAMX_COLORS.keys() else hgpltutils.LIGAMX_COLORS['None']
                   for team in teams_dict.keys()]

    fig, ax = plt.subplots()

    bar_container = plt.bar(teams_dict.keys(), teams_dict.values(), color=team_colors)

    fontdict = {'fontsize': title_fontsize}
    ax.set_title(title, fontdict=fontdict)

    # ------------------------------------------------
    #  Tweaks
    # ------------------------------------------------

    #  Hide tick marks and y labels
    # ------------------------------------------------
    ax.tick_params(
        axis='both',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        left=False,
        right=False,
        labelleft=False)

    #  Hide spines
    # ------------------------------------------------
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    # ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)

    #  Display the value
    # ------------------------------------------------
    for rec in bar_container.patches:
        value_x_pos = rec.get_x() + rec.get_width() / 2
        value_y_pos = rec.get_height() / 2
        value_str = str(int(rec.get_height()))

        ax.annotate(value_str, xy=(value_x_pos, value_y_pos),
                    horizontalalignment='center', verticalalignment='center',
                    size=25, color='w')

        # Tunning the patch (rectangle)
        rec.set_alpha(0.8)

    plt.show()

    return fig, ax


def add_teams_logos_to_axes_pie(ax, team1, team2, texts, logos_pos='up', zoom=0.1, roffset=0.25):
    """
    Add logos to the Axes.Pie wins plot.

    Args:
        ax (matplotlib.Axes):
        team1 (str): Must correspond to the first element on the 'texts' list.
        team2 (str): Must correspond to the last element on the 'texts' list.
        texts (list of matplotlib.text.Text): Object returned by matplotlib.axes.Axes.pie. Position of the logo
                                              depends on this data
        logos_pos (str, opt): 'up'; upper corners. 'middle': sides of the plot.
                              'low': lower corners. 'data': aligned with the values on the plot.
        zoom (float, opt): Scale applied to the logo image.
        roffset (float, opt): Offset radial to the Pie plot. Used to separate more the logo if needed.

    Returns:
        None

    """
    assert (len(texts) == 3)

    theta = np.pi / 8
    coord_x = np.cos(theta)
    coord_y = np.sin(theta)

    # print("coords: ", coord_x, coord_y)

    if 'up' == logos_pos:
        logo_pos = ((-coord_x - roffset, coord_y + roffset),
                    (coord_x + roffset, coord_y + roffset))
    elif 'center' == logos_pos:
        logo_pos = ((-1 - roffset, 0), (1 + roffset, 0))
    elif 'low' == logos_pos:
        logo_pos = ((-coord_x - roffset, -coord_y - roffset), (coord_x + roffset, -coord_y - roffset))
    elif 'data' == logos_pos:
        logo_pos = (texts[0].get_position(), texts[2].get_position())
    else:
        raise ValueError("Invalid value for 'logos_pos' parameter.")

    # print('logo_pos: ', logo_pos)

    for i, team in enumerate((team1, team2)):
        team_logo_filename = os.path.join(hgpltutils.LOGOS_DIR, hgpltutils.LOGOS_DICT[team])

        # Load the image data
        with mplib.cbook.get_sample_data(team_logo_filename) as file:
            arr_img = plt.imread(file, format='png')

        imagebox = mplib.offsetbox.OffsetImage(arr_img, zoom=zoom)
        imagebox.image.axes = ax

        # print(' logo_pos[i]: ', logo_pos[i])
        box_x, box_y = logo_pos[i]

        if logos_pos == 'data':
            if box_x > 0:
                box_x += roffset
            else:
                box_x -= roffset

            if box_y > 0:
                box_y += roffset
            else:
                box_y -= roffset

        boxprops = {"facecolor": "none", "edgecolor": "none"}

        ab = mplib.offsetbox.AnnotationBbox(imagebox, (box_x, box_y),
                                            frameon=False,
                                            xybox=(box_x, box_y),
                                            xycoords='data',
                                            boxcoords="data",
                                            pad=0.5,
                                            bboxprops=boxprops)

        ax.add_artist(ab)


def add_teams_logos_to_bar_plot(ax, team1, team2, location='bottom', offset=0.2, scale=0.1):
    """

    Args:
        ax:
        team1:
        team2:
        location:
        offset:
        scale:

    Returns:

    """
    for i, team in enumerate((team1, team2)):
        team_logo_filename = os.path.join(hgpltutils.LOGOS_DIR, hgpltutils.LOGOS_DICT[team])

        # Load the image data
        with mplib.cbook.get_sample_data(team_logo_filename) as file:
            arr_img = plt.imread(file, format='png')

        imagebox = mplib.offsetbox.OffsetImage(arr_img, zoom=scale)
        imagebox.image.axes = ax

        patch = ax.patches[i]

        if location == 'bottom':
            img_width_px, img_height_px = imagebox.image.get_size()

            box_x = patch.get_x() + (patch.get_width() / 2)
            box_y = patch.get_y() - offset
            # box_y = patch.get_y() + (img_height_px * scale) / 10
            fig = ax.get_figure()
            # fig.subplots_adjust(bottom=0.4)
            # print(ax.viewLimBbox)
            # print(ax.dataLim)

        ab = mplib.offsetbox.AnnotationBbox(imagebox, (box_x, box_y),
                                            frameon=False,
                                            xybox=(box_x, box_y),
                                            xycoords='data',
                                            boxcoords="data",
                                            pad=0.5)

        ax.add_artist(ab)
    # -----------------------------------------------------------------------------------------------------------------


def add_teams_logos_to_bar_plot_title(ax, team1, team2, ref_x, ref_y, offset_x, offset_y, scale=0.1):
    """

    Args:
        ax:
        team1:
        team2:
        location:
        offset:
        scale:

    Returns:

    """
    for i, team in enumerate((team1, team2)):
        team_logo_filename = os.path.join(hgpltutils.LOGOS_DIR, hgpltutils.LOGOS_DICT[team])

        # Load the image data
        with mplib.cbook.get_sample_data(team_logo_filename) as file:
            arr_img = plt.imread(file, format='png')

        imagebox = mplib.offsetbox.OffsetImage(arr_img, zoom=scale)
        imagebox.image.axes = ax

        patch = ax.patches[i]


        img_width_px, img_height_px = imagebox.image.get_size()

        # Place the icons at each side of the "vs" text
        if i == 0:
            box_x = ref_x - img_width_px * scale - offset_x
        else:
            box_x = ref_x + img_width_px * scale + offset_x

        box_y = ref_y + offset_y

        fig = ax.get_figure()

        ab = mplib.offsetbox.AnnotationBbox(imagebox, (box_x, box_y),
                                            frameon=False,
                                            xybox=(box_x, box_y),
                                            xycoords='figure pixels',
                                            boxcoords='figure pixels',
                                            pad=0.5)

        ax.add_artist(ab)
    # -----------------------------------------------------------------------------------------------------------------


def bar_plot_wins_liguilla(df, team1, team2, title='', title_fontsize=20, fig_size=(10, 10), save_fig=None):
    """

    Returns:
        tuple of objects: (matplotlib.figure.Figure, matplotlib.axes.Axes)

    """
    # Create a list of dictionaries that contain the results for each phase. The first element contains the info of
    # ALL phases.
    direct_matches_dicts = [hgdatautils.get_wins_direct_matches_dict(df, team1, team2, phase=opt)
                            for opt in ('cuartos', 'semis', 'finales')]

    # Create a np.Array 2D. Keep only home and away results.
    #   NOTE: here we order in a different than the dictionary since when it's plotted
    #         we want to display team1 wins and team2 wins next to each other separated
    #         by the draws.
    direct_matches = np.array([(d[team1], d['Empate'], d[team2]) for d in direct_matches_dicts])

    # Create a new ndarray for granulated wins
    #     direct_matches is a m x n x o ndarray
    #         m: 6 - all results + number of phases, i.e., All, Regular, Classif, Quarters, Semis, Finals
    #         n: 3 - Each team: team1, team2, draw
    #         o: 2 - wins at home, wins away, NOTE: should I include the total here to avoid computing it later?
    granulated_wins = np.array([direct_matches[1:, 0, 2],
                                direct_matches[1:, 1, 2],
                                direct_matches[1:, 2, 2]])

    # phases_colors = plt.cm.get_cmap('Greys', 7)

    fig, ax2 = plt.subplots()
    fig.set_size_inches(5, 10)
    # fig.tight_layout()
    # fig.suptitle(f'{team1} vs {team2}\nResultados de {df.shape[0]} duelos en torneos cortos', fontsize=title_fontsize)

    # direct_matches_dicts are ordered in the following manner: [Cuartos, Semis, Finales]
    stage_accum = np.array([0, 0, 0])
    teams = [team1, team2, 'Empates']

    # print(direct_matches_dicts)

    colors = ['#ed8e63', '#f6a24e', '#e97140']

    for i, stage_results in enumerate(direct_matches_dicts):
        # print(stage_results)

        stage_results_array = np.array([stage_results[team1][2],
                                        stage_results[team2][2],
                                        stage_results['Empate'][2]])

        ax2.bar(teams, stage_results_array, bottom=stage_accum, color=colors[i], clip_box=[[-10, 10], [-10, 10]])

        stage_accum += stage_results_array

    #
    #  Tweaks
    #
    ax2.set_ylim([0, 10])

    #  Hide some spines
    # --------------------------------------------------------
    ax2.spines['top'].set_visible(False)
    ax2.spines['left'].set_visible(False)
    ax2.spines['bottom'].set_visible(True)
    ax2.spines['right'].set_visible(False)

    #  Hide y ticks
    # --------------------------------------------------------
    ax2.tick_params(axis='both',  # changes apply to the x-axis
                    which='both',  # both major and minor ticks are affected
                    bottom=False,  # ticks along the bottom edge are off
                    top=False,  # ticks along the top edge are off
                    left=False,
                    right=False,
                    labelleft=False)

    #  Display the value for each bar
    # --------------------------------------------------------
    for patch in ax2.patches:
        if patch.get_height() != 0:
            center_x = patch.get_width() / 2
            center_y = patch.get_height() / 2
            text_x = patch.get_x() + center_x
            text_y = patch.get_y() + center_y

            ax2.annotate(patch.get_height(),
                         xy=(text_x, text_y),
                         ha='center', va='center',
                         fontsize='xx-large', fontweight='demi', color='w')

    add_teams_logos_to_bar_plot(ax2, team1, team2)

    plt.show()

    # #
    # #
    # #
    # wedge_size = 0.3
    # text_size_outer = 20
    # text_size_inner = 18
    # text_pos_outer = 0.85
    # text_pos_inner = 0.75
    #
    # # ------------------------------------------------
    # #  Plot 2 - Total wins + phases
    # # ------------------------------------------------
    # ax2.set_title(f'Juegos de liguilla.', fontdict={'fontsize': title_fontsize - 4})
    #
    # print(direct_matches[0])
    # print(direct_matches[0, :, 2])
    # print(direct_matches[0, :, 2].flatten())
    # patches3, texts3, autotexts3 = ax2.pie(direct_matches[0, :, 2].flatten(), labels=(team1, 'Empates', team2),
    #                                        autopct=lambda pct: func(pct, direct_matches[0, :, 2].flatten()),
    #                                        radius=1, wedgeprops=dict(width=wedge_size, edgecolor='w'),
    #                                        colors=results_colors, startangle=90, pctdistance=text_pos_outer,
    #                                        textprops=dict(color="w", fontsize=text_size_outer))
    #
    # patches4, texts4, autotexts4 = ax2.pie(granulated_wins.flatten(),
    #                                        # labels=('R', 'C', 'Q', 'S', 'F')*granulated_wins.shape[0],
    #                                        autopct=lambda pct: func(pct, granulated_wins),
    #                                        radius=1 - wedge_size, wedgeprops=dict(width=wedge_size, edgecolor='w'),
    #                                        colors=phases_colors(range(2, 8)), startangle=90, pctdistance=text_pos_inner,
    #                                        textprops=dict(color="w", fontsize=text_size_inner))
    #
    # for wedge3 in patches3:
    #     wedge3.set_alpha(0.8)
    #
    # for wedge in patches4:
    #     wedge.set_alpha(0.75)

    return fig, ax2


def plot_wins(df, team1, team2, phase='regular', title='', title_fontsize=20, fig_size=(10,10), save_fig=None):
    """
    Plot bars depicting the wins of each team and their draws in each phase.

    Args:
        df (pandas.DataFrame):
        team1 (str):
        team2 (str):
        phase (str, opt): Only data having this phase will be selected from the DataFrame.
                          Valid values: 'regular' (default), 'repesca', 'cuartos', 'semis', 'finales', 'liguilla',
                          'directa', 'all'.
        title (str, opt):
        title_fontsize (int, opt):
        fig_size (tuple int, opt):
        save_fig (str, opt):

    Returns
        tuple of objects: (matplotlib.figure.Figure, matplotlib.axes.Axes)
    """

    # Create a list of dictionaries that contain the results for each phase. The first element contains the info of
    # ALL phases.
    team1_wins = []
    team2_wins = []
    draws = []
    # team1_wins = [[]]
    # team2_wins = [[]]
    # draws = [[]]
    phases = ['Regular', 'Repesca', 'Cuartos', 'Semis', 'Finales']

    if phase == 'all':
        for ph in phases:
            results = hgdatautils.get_wins_direct_matches_dict(df, team1, team2, phase=ph.lower())
            # team1_wins.append(results[team1][-1])  # Last element contains the total
            # team2_wins.append(results[team2][-1])  # Last element contains the total
            # draws.append(results['Empate'][-1])  # Last element contains the total
            team1_wins.append(results[team1][:2])  # Get team1 wins as home and away
            team2_wins.append(results[team2][:2])  # Get team2 wins as home and away
            draws.append(results['Empate'][:2])  # Get team 1 and team2 draws in their respective homes
    else:
        results = hgdatautils.get_wins_direct_matches_dict(df, team1, team2, phase=phase)
        # team1_wins.append(results[team1][-1])  # Last element contains the total
        # team2_wins.append(results[team2][-1])  # Last element contains the total
        # draws.append(results['Empate'][-1])  # Last element contains the total
        team1_wins.append(results[team1][:2])  # Get team1 wins as home and away
        team2_wins.append(results[team2][:2])  # Get team2 wins as home and away
        draws.append(results['Empate'][:2])  # Get team 1 and team2 draws in their respective homes

    # Reuse the variable names. Convert them to Numpy arrays
    team1_wins = np.array(team1_wins)  # Last element contains the total wins
    team2_wins = np.array(team2_wins)  # Last element contains the total wins
    draws = np.array(draws)  # Last element contains the total wins

    # print(team1_wins)
    # print(team2_wins)
    # print(draws)

    # ------------------------------------------------
    #  Plot - Total wins + home and away
    # ------------------------------------------------
    fig, ax1 = plt.subplots()
    if fig_size is not None:
        # Previous default value was (10, 10), let's see if removing this corrects the title from being cut out
        fig.set_size_inches(fig_size)

    # Background color
    # fig.set_facecolor('#f6f7f2')
    fig.set_facecolor('#f0f0f0')  # facecolor applies only when displayed on plt. Transparent when image is saved.
    ax1.set_facecolor('#f0f0f0')  # facecolor applies only when displayed on plt. Transparent when image is saved.

    # Common settings

    # TODO: I think this has to be replaced by text that we relocate.
    if title == '':
        # title = f'{hgpltutils.LIGAMX_NAMES[team1]} vs {hgpltutils.LIGAMX_NAMES[team2]}\n' \
        #         f'{team1_wins.sum() + team2_wins.sum() + draws.sum()} juegos de {hgpltutils.get_phase_text_for_title(phase)} en torneos cortos\n'
        title = f'Resultados como local y visitante en {team1_wins.sum() + team2_wins.sum() + draws.sum()} juegos de\n' \
                f'{hgpltutils.get_phase_text_for_title(phase)} en torneos cortos\n\n' \
                f'\nvs'
                # f'{hgpltutils.LIGAMX_NAMES[team1]} vs {hgpltutils.LIGAMX_NAMES[team2]}'

    title_obj = ax1.set_title(title, fontdict={'fontsize': title_fontsize}, pad=80, wrap=True)
    title_w = title_obj.get_window_extent(renderer=fig.canvas.get_renderer()).width
    title_h = title_obj.get_window_extent(renderer=fig.canvas.get_renderer()).height

    print(title_w)
    print(title_h)


    n_phases = len(team1_wins)
    bar_width = .31
    ph = np.arange(n_phases)
    accum = np.zeros((team1_wins.shape[0], 3))
    rects_t1 = []
    rects_d = []
    rects_t2 = []
    alpha = 1

    # Here we only have two fields, home and away
    for i in range(team1_wins.shape[1]):
        rects_t1.append(ax1.bar(ph + 0 * bar_width, team1_wins[:,i], bottom=accum[:,0],
                           width=bar_width, color=hgpltutils.get_team_color(team1), edgecolor='#ffffffff', linewidth=2, alpha=alpha))
        accum[:, 0] += team1_wins[:, i]

        rects_d.append(ax1.bar(ph + 1 * bar_width, draws[:,i], bottom=accum[:,1],
                          width=bar_width, color=hgpltutils.get_team_color('None'), edgecolor='#ffffffff', linewidth=2, alpha=alpha))
        accum[:, 1] += draws[:, i]

        rects_t2.append(ax1.bar(ph + 2 * bar_width, team2_wins[:,i], bottom=accum[:,2],
                           width=bar_width, color=hgpltutils.get_team_color(team2), edgecolor='#ffffffff', linewidth=2, alpha=alpha))
        accum[:, 2] += team2_wins[:, i]

        # alpha -= 0.15

    if 'all' == phase:
        # print(ph)
        # print(phases)
        ax1.set_xticks(ph + bar_width)
        ax1.set_xticklabels(phases)
    else:
        ax1.set_xticks(ph, phase)

    # ------------------------------------------------
    #  Add logos in plot
    # ------------------------------------------------
    # add_teams_logos_to_bar_plot(ax1, team1, team2, location='bottom', scale=0.1)
    plot_img_ref_x = 794 / 2   # pixels
    # plot_img_ref_x = 794  # pixels
    plot_img_ref_y =  1072 * (7 / 8)  # pixels
    # add_teams_logos_to_bar_plot_title(ax1, team1, team2, title_w/2, scale=0.1)
    add_teams_logos_to_bar_plot_title(ax1, team1, team2, plot_img_ref_x, plot_img_ref_y, offset_x=20, offset_y=-20, scale=0.15)

    #  Hide tick marks and y labels
    # ------------------------------------------------
    ax1.tick_params(
        axis='both',  # changes apply to the x-axis
        which='both',  # both major and minor ticks are affected
        bottom=False,  # ticks along the bottom edge are off
        top=False,  # ticks along the top edge are off
        left=False,
        right=False,
        labelleft=False)

    #  Hide spines
    # ------------------------------------------------
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.spines['left'].set_visible(False)

    #  Display the value for each bar
    # --------------------------------------------------------
    # Triple nested loop, but it is what it is.
    for i, recs in enumerate([rects_t1, rects_d, rects_t2]):
        if 0 == i:
            clr = hgpltutils.get_team_text_color(team1)
        elif 1 == i:
            clr = hgpltutils.get_team_text_color('None')
        else:
            clr = hgpltutils.get_team_text_color(team2)

        for cont in recs:
            for rec in cont:
                if 0 != rec.get_height():
                    value_x_pos = rec.get_x() + rec.get_width() / 2
                    value_y_pos = rec.get_y() + rec.get_height() / 2 - 0.05 # 0.05 is a small correction because values look a litle biased to the top
                    value_str = str(int(rec.get_height()))

                    ax1.annotate(value_str, xy=(value_x_pos, value_y_pos),
                                 horizontalalignment='center', verticalalignment='center',
                                 size=14, fontweight='demi', color=clr)

    # ------------------------------------------------
    # Save the figure
    # ------------------------------------------------
    if save_fig:
        hgpltutils.create_dir(HOME_AWAY_PLOTS_REL_DIR)
        filename = os.path.join(HOME_AWAY_PLOTS_REL_DIR, f'{team1}_v_{team2}_wins_{phase}.png')
        # print(f'Saving image in: {filename}')
        fig.savefig(filename, transparent=True, bbox_inches='tight')

    plt.show()

    return fig, ax1
