import os
import scrapy
import numpy as np
import datetime
import glob
import contextlib


# ----------------------------------------------------------------------------------------------------------------------
#  Resources to take a look at
# ----------------------------------------------------------------------------------------------------------------------
# https://www.w3schools.com/cssref/css_selectors.asp
#

class VEFCalendarSpider(scrapy.Spider):
    """
    Spider class to get all fixtures results from http://www.vivoelfutbol.com.mx
    """

    # -----------------------------------------------------------------------------------------------------------------
    #  CLASS CONSTANTS
    # -----------------------------------------------------------------------------------------------------------------

    # Main URL, goes directly to the calendar results
    URL_VEF_MAIN_FIXTURE = 'http://www.vivoelfutbol.com.mx/jornada.php?'

    LIGAMX_FILENAME = "vef-ligamx-short-seasons"
    LIGAMX_FILE = "vef-ligamx-short-seasons.csv"
    LIGAMX_FILE_HEADER = 'season,date_ext,time_short,date,time,' \
                         'phase,phase_int,round,team1,team1_score,team2,team2_score,stadium\n'

    # Working directories
    WDIR_ALL_SINGLE = './artifacts/all-fixtures-single/'
    WDIR_ALL_MULTI = './artifacts/all-fixtures-multi/'
    WDIR_LATEST = './artifacts/latest-fixture/'

    # Phases will be expressed also as a number:
    #    0: regular
    #   -1: classification(repechaje, if any), -2 classification 2 (repechaje 2, if any), ...
    #    1: final, 2:semifinal, 4:quarters, 8:eights, 16:sixteenths, 32:thirty-seconds, ...
    PHASES = {'Regular': 0,
              'Repechaje': -1,
              'Liguilla Cuartos De Final': 4, 'Cuartos De Final': 4,
              'Semi-Finals': 2, 'Semifinal': 2,
              'Final': 1}

    #
    # Options
    #
    OPTS = {'all_single_file': 1, 'all_multi_file': 2, 'latest': 3}

    OPT_ALL_SINGLE = 1
    OPT_ALL_MULTI = 2
    OPT_LATEST = 3

    # -----------------------------------------------------------------------------------------------------------------
    #  CLASS ATTRIBUTES
    # -----------------------------------------------------------------------------------------------------------------

    name = 'vef-fixtures-spider'

    # -----------------------------------------------------------------------------------------------------------------
    #  METHODS
    # -----------------------------------------------------------------------------------------------------------------

    def __init__(self, opt='all_single_file', *args, **kwargs):
        """

        Args:
            *args:
            opts (optional): str - 'all_multi', 'all_sing', 'latest'
            **kwargs:

        Notes:
            'all_single_file': Get ALL fixtures and write the info in a sigle file.
            'all_multi_file': Get ALL fixtures and write the info in separate files, one for each season
            'latest':   Get ONLY the latest fixture.
        """
        super().__init__(*args, **kwargs)

        self.opt = VEFCalendarSpider.OPTS[opt]
        self.rel_wdir = None

        # Create the required directories
        self._create_directories()

        #
        # Remove files if they exist
        #
        self._clean_directories()

    def start_requests(self):
        """
        Start crawling process.

        Yields:
            Scrapy.Request
        """
        # Create the CSV file and add the header ONLY if it's when getting ALL fixtures and writing a SINGLE file.
        if self.opt == self.OPT_ALL_SINGLE:
            with self._my_change_dir(self.rel_wdir):
                with open(self.LIGAMX_FILE, 'a') as f:
                    f.write(self.LIGAMX_FILE_HEADER)

        if self.opt == self.OPT_LATEST:
            yield scrapy.Request(url=self.URL_VEF_MAIN_FIXTURE, callback=self.parse_fixture)
        else:
            yield scrapy.Request(url=self.URL_VEF_MAIN_FIXTURE, callback=self.parse_main_calendar_page)

    def parse_main_calendar_page(self, response):
        """Build the URLs but only up to season, since the fixture may include a Classification phase that is not available
        to every season

        Args:
            response (Scrapy.Response):

        Yields:
            Scrapy.Response

        """
        # LigaMX is 1
        # CopaMX is 14
        tournament = []
        seasons = []

        combo_box_selectors = response.css('div.calendario > div.combo > select')
        TOURNAMENT_COMBO_BOX_IDX = 0
        SEASON_COMBO_BOX_IDX = 1

        # Check that we the second element is the combo box with the seasons
        if combo_box_selectors[TOURNAMENT_COMBO_BOX_IDX].attrib['name'] == 'to':
            tournament = combo_box_selectors[TOURNAMENT_COMBO_BOX_IDX].css('option::attr(value)').getall()

        # Check that we the second element is the combo box with the seasons
        if combo_box_selectors[SEASON_COMBO_BOX_IDX].attrib['name'] == 'te':
            seasons = combo_box_selectors[SEASON_COMBO_BOX_IDX].css('option::attr(value)').getall()

        for season in seasons:
            url = f'{response.url}?to={tournament}&te={season.strip()}'
            yield response.follow(url=url, callback=self.parse_main_fixture)

    def parse_main_fixture(self, response):
        """
        Build the URLs to access the pages with the information desired.

        Args:
            response: Scrapy.Response
        Returns:
            None

        Notes:
            The URLs have the following structure:
            top.location.href="jornada.php?to=X&te=Y&jo=Z";
                - X: <select> item. class="seljor". name="to". LigaMX is 1, CopaMX is 14
                - Y: <select> item. class="seljor". name="te".
                - Z: <select> item. class="seljor". name="jo"
        """
        FIXTURE_COMBO_BOX_IDX = 2

        fixtures = []

        combo_box_selectors = response.css('div.calendario > div.combo > select')

        # Check that the third element is the combo box with the fixture/rounds
        if combo_box_selectors[FIXTURE_COMBO_BOX_IDX].attrib['name'] == 'jo':
            fixtures = combo_box_selectors[FIXTURE_COMBO_BOX_IDX].css('option::attr(value)').getall()

        # Now, create the URLs
        urls = []

        for fixture in fixtures:
            url = f'{response.url}&jo={fixture.strip()}'
            # url = f"{VEFCalendarSpider.URL_VEF_MAIN_FIXTURE}to={tournament}&te={season.strip()}&jo={fixture.strip()}"
            urls.append(url)
            # print(url)
            yield response.follow(url=url, callback=self.parse_fixture)

        # --------------------
        #  HG_DBG
        # --------------------
        # dbg_url = 'http://www.vivoelfutbol.com.mx/jornada.php?to=1&te=57&jo=7'
        # dbg_url = 'http://www.vivoelfutbol.com.mx/jornada.php?to=1&te=57&jo=9999'
        # yield response.follow(url=dbg_url, callback=self.parse_fixture)

    def parse_fixture(self, response):
        """
        Parse the fixture page.

        Args:
            response: Scrapy.Response

        Returns:
            None

        Notes:
            Page structure:
            div.calendario
                div.combo                   # Here is the information to build the URLs
                    div.tit                 # It's just the title of the table: Local Visita Hora
                    div.tif::text           # This is the date. It's a problem since it could be shared among some div.det's. Need to extract tif's and det's sequentially
                    div.det                 # The whole row with the information
                        div.eql > a::text   # Home team
                        div.mar > a::text   # Result: home team score - away team score
                        div.eqv > a::text   # Away team
                        div.hor::text       # Hour
        """
        calendar_selector = response.css('div.calendario')
        # print("  HG_DBG: calendar_selector: ", type(calendar_selector))

        # The options selected have a special empty attribute called selected:
        #       <option selected="" value="57">Apertura 2012</option>
        season = calendar_selector.css('select[name="te"] > option[selected]::text').get()
        fixture = calendar_selector.css('select[name="jo"] > option[selected]::text').get()
        fixture_tmp = int(calendar_selector.css('select[name="jo"] > option[selected]::attr(value)').get())

        phase = self._helper_xlate_fixture_to_phase_str(fixture_tmp)
        phase_int = self._helper_xlate_fixture_to_phase_int(fixture_tmp)

        rows = calendar_selector.css('div.tif, div.det')
        # print("  HG_DBG: rows: ", type(rows))

        match_date_str = ""
        match_datetime = None
        csv_lines = []

        for row in rows:
            # score1 = np.NaN
            # score2 = np.NaN

            # If we have a div.tif, extract the date info and continue to the next element in the list
            if row.attrib['class'] == 'tif':
                match_date_str = row.css('::text').get().strip()
                match_datetime = self._helper_convert_str_to_datetime(match_date_str)
                continue

            # Extract the data regarding the match
            if row.attrib['class'] == 'det':
                team1_str = row.css('div.eql > a::text').get().strip()
                team2_str = row.css('div.eqv > a::text').get().strip()
                time_str = row.css('div.hor::text').get().strip()
                result_str = row.css('div.mar > a::text').get().strip()

                # Parse the result string
                score1, score2 = self._helper_parse_score_str(result_str)

                # Update the time now that we have that information
                match_datetime = self._helper_update_time(match_datetime, time_str)

                # print(match_date_str)
                # csv_data_point = f"{match_date_str},{time_str},{team1_str},{score1},{team2_str},{score2}\n"
                csv_data_point = f"{season},{match_date_str},{time_str}," \
                                 f"{match_datetime.date()},{match_datetime.time()}," \
                                 f"{phase},{phase_int},{fixture}," \
                                 f"{team1_str},{score1},{team2_str},{score2}\n"
                csv_lines.append(csv_data_point)
                # print(csv_data_point)

        # Write what we have to our CSV file
        with self._my_change_dir(self.rel_wdir):
            if self.opt == self.OPT_LATEST or self.opt == self.OPT_ALL_MULTI:
                with open(f'{self.LIGAMX_FILENAME}-{season}.csv', 'a') as f:
                    if self.opt == self.OPT_LATEST:
                        f.write(self.LIGAMX_FILE_HEADER)
                    f.writelines(csv_lines)

            else:
                with open(self.LIGAMX_FILE, 'a') as f:
                    f.writelines(csv_lines)

    # -----------------------------------------------------------------------------------------------------------------
    #  HELPER FUNCTIONS
    # -----------------------------------------------------------------------------------------------------------------

    def _clean_directories(self):
        """
        Remove the output files created in the SAME previous run.

        Returns:
            None

        """
        if self.opt == self.OPT_ALL_SINGLE:
            self.rel_wdir = self.WDIR_ALL_SINGLE

            with self._my_change_dir(self.rel_wdir):
                if self.opt == self.OPT_ALL_SINGLE:
                    # Delete the file if it exists
                    if os.path.exists(self.LIGAMX_FILENAME + '.csv'):
                        os.remove(self.LIGAMX_FILENAME + '.csv')

        elif self.opt == self.OPT_ALL_MULTI:
            self.rel_wdir = self.WDIR_ALL_MULTI

            with self._my_change_dir(self.rel_wdir):
                # Delete all files in ./all-fixtures/
                # files = glob.glob(VEFCalendarSpider.LIGAMX_FILENAME + '-[[:digit:]]*.csv')
                files = glob.glob(self.LIGAMX_FILENAME + '-*.csv')
                for f in files:
                    os.remove(f)

        elif self.opt == self.OPT_LATEST:
            self.rel_wdir = self.WDIR_LATEST

            with self._my_change_dir(self.rel_wdir):
                # Delete the file if it exists
                if os.path.exists(self.LIGAMX_FILENAME + '-latest.csv'):
                    os.remove(self.LIGAMX_FILENAME + '-latest.csv')

    def _create_directories(self):
        """
        Create the directories where the output files will be placed in.

        Returns:
            None

        """
        if not os.path.exists(self.WDIR_ALL_SINGLE):
            os.makedirs(self.WDIR_ALL_SINGLE)

        if not os.path.exists(self.WDIR_ALL_MULTI):
            os.makedirs(self.WDIR_ALL_MULTI)

        if not os.path.exists(self.WDIR_LATEST):
            os.makedirs(self.WDIR_LATEST)

    @contextlib.contextmanager
    def _my_change_dir(self, dir):
        """
        Change the directory to the one specified in dir. Context manager.

        Args:
            dir: str - Directory to change to

        Returns:
            None

        """
        saved_cwd = os.getcwd()
        os.chdir(dir)

        yield

        os.chdir(saved_cwd)

    @staticmethod
    def _helper_xlate_fixture_to_phase_str(fixture_tmp):
        """
        TODO: Consider removing this function, and the data entry, since it's better to only have the numeric value
              PHASE_INT
        Fixture in this site are marked as follows:
            Regular match:  0 < fixture_tmp < 88
            Classification: fixture_tmp == 88
            Quarters:       fixture_tmp == 9999
            Semifinals:     fixture_tmp == 99999
            Finals:         fixture_tmp == 999999

        Args:
            fixture_tmp:

        Returns:
            str - Any of 'Regular', 'Semifinales
        """
        # print(fixture_tmp)
        if fixture_tmp == 88:
            xlated_fixture = 'Repechaje'  # Classification (repechaje)
        elif fixture_tmp == 9999:
            xlated_fixture = 'Cuartos'  # Quarters
        elif fixture_tmp == 99999:
            xlated_fixture = 'Semifinales'  # Semifinals
        elif fixture_tmp == 999999:
            xlated_fixture = 'Final'  # Finals
        else:
            xlated_fixture = 'Regular'  # Regular match by default

        return xlated_fixture

    @staticmethod
    def _helper_xlate_fixture_to_phase_int(fixture_tmp):
        """
        Translate the  fixture_tmp into a corresponding phase value.

        Args:
            fixture_tmp: int - phase code used by the site.

        Returns:
            int - Translated phase of the fixture

        Notes:
            Fixture in this site are marked as follows:
                Regular match:  0 < fixture_tmp < 88
                Classification: fixture_tmp == 88
                Quarters:       fixture_tmp == 9999
                Semifinals:     fixture_tmp == 99999
                Finals:         fixture_tmp == 999999
        """

        if fixture_tmp == 88:
            xlated_fixture = -1  # Classification (repechaje)
        elif fixture_tmp == 9999:
            xlated_fixture = 4  # Quarters
        elif fixture_tmp == 99999:
            xlated_fixture = 2  # Semifinals
        elif fixture_tmp == 999999:
            xlated_fixture = 1  # Finals
        else:
            xlated_fixture = 0  # Regular match by default

        return xlated_fixture

    @staticmethod
    def _helper_update_time(datetime_obj, time_str):
        """
        Update the time of a datetime object with the information provided by time_str.

        Args:
            datetime_obj: Datetime.datetime - Object to be modified
            time_str: str - A string with the time format as indicated above.

        Returns
            Datetime.datetime -

        Notes:
        Since this is a helper function, we assume time_str has the following structure
            24 hour format
            hh:mm:ss:uu
        """
        new_datetime_obj = datetime_obj

        if datetime_obj is not None and time_str is not None:
            tokens = time_str.split(':')
            nb_tokens = len(tokens)

            if nb_tokens == 1:
                new_datetime_obj = datetime_obj.replace(hour=int(tokens[0]))
            elif nb_tokens == 2:
                new_datetime_obj = datetime_obj.replace(hour=int(tokens[0]), minute=int(tokens[1]))
            elif nb_tokens == 3:
                new_datetime_obj = datetime_obj.replace(hour=int(tokens[0]), minute=int(tokens[1]),
                                                        second=int(tokens[2]))
            elif nb_tokens == 4:
                new_datetime_obj = datetime_obj.replace(hour=int(tokens[0]), minute=int(tokens[1]),
                                                        second=int(tokens[2]), microsecond=int(tokens[3]))

        return new_datetime_obj

    @staticmethod
    def _helper_convert_str_to_datetime(date_str):
        """
        Return a datetime object based on date_str.

        Args:
            date_str: str - A string expressed in a format as in the Notes section.

        Returns:
            Datetime.datetime

        Notes:
        Since this is a helper function, we assume that we always have the date_str in the following format:
            Date format in spanish
            Www D de Mmm de YYYY
            Vie 8 de Ene de 2021
        """
        short_days_dict = {'Dom': 'Sun', 'Lun': 'Mon', 'Mar': 'Tue', 'Mie': 'Wed', 'Jue': 'Thu', 'Vie': 'Fri',
                           'Sab': 'Sat'}
        short_months_dict = {'Ene': 'Jan', 'Feb': 'Feb', 'Mar': 'Mar', 'Abr': 'Apr', 'May': 'May', 'Jun': 'Jun', \
                             'Jul': 'Jul', 'Ago': 'Aug', 'Sep': 'Sep', 'Oct': 'Oct', 'Nov': 'Nov', 'Dic': 'Dec'}

        # Here we assume we always have the format indicated in the docstring, therefore, 0 is day of week, 1 is the day
        # of the month, 3 is the month, 5 is the year
        tokens = date_str.split(" ")

        week_day = short_days_dict[tokens[0]]
        day = tokens[1]
        month = short_months_dict[tokens[3]]
        year = tokens[5]

        # Create a datetime object
        # https://docs.python.org/3.7/library/time.html#time.strptime
        date = f"{week_day} {day} {month} {year}"
        datetime_obj = datetime.datetime.strptime(date, "%a %d %b %Y")

        return datetime_obj

    @staticmethod
    def _helper_parse_score_str(final_score_str):
        """
        Parse a string of the form 'team1_score - team2_score' and convert each result to integers

        Args:
            final_score_str: str - A string of the form 'team1_score - team2_score'

        Returns
            tuple of int - A tuple with [team1_score, team2_score]. Integers if result was found. NaN otherwise.
        """
        score1 = np.NaN
        score2 = np.NaN

        dash_idx = final_score_str.find('-')

        # If found
        if dash_idx != -1:
            score1_str = final_score_str[:dash_idx].strip()
            score2_str = final_score_str[dash_idx + 1:].strip()

            # print('  HG_DBG: score1_str: "{}", score2_str: "{}"'.format(score1_str, score2_str))
            if (score1_str is not None and score1_str.strip()) and (
                    score2_str is not None and score2_str.strip()):
                # print(score1_str, score2_str)
                score1 = int(score1_str)
                score2 = int(score2_str)

        return score1, score2

# http://www.vivoelfutbol.com.mx/jornada.php?to=1&te=55&jo=1
# http://www.vivoelfutbol.com.mx/jornada.php?to=1&te=55&jo=123
